WHAT DO
-------
Parse text field (field type = text_with_summary) into entity reference taxonomy term field
(field type = entity_reference)
on event create and update entity.

SETTING
-------
field type entity_reference
admin/structure/types/manage/{bundle}/fields/{type}.{bundle}.{field_name}

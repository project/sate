<?php

function sate_filter_clear_empty($var) {
  return trim($var) != '';
}

function sate_filter_set_punctuation($item) {
  return ' ' . $item;
}

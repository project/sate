<?php


namespace Drupal\sate;

use Drupal\taxonomy\Entity\Term;

class Sate {

  public static function useAutoTerm($field, $field_settings, $is_new) {
    return
      !empty($field_settings['target_type'])
      &&
      $field_settings['target_type'] == 'taxonomy_term'
      &&
      $field->getThirdPartySetting('sate', 'is_auto_term')
      &&
      ($is_new || $field->getThirdPartySetting('sate', 'use_update', TRUE));
  }

  public static function getSafeString($text) {
    $text = preg_replace('/[\f\n\r\t\v]+/', "\n", $text);
    $result = explode("\n", $text);
    module_load_include('inc', 'sate', 'sate.filter');
    $result = array_filter($result, 'sate_filter_clear_empty');
    $result = array_map('sate_filter_set_punctuation', $result);
    return array_unique($result);
  }

  public static function getReplaceString($text) {
    $result = [];
    $text = preg_replace('/[\f\n\r\t\v]+/', "\n", $text);
    $lines = explode("\n", $text);
    foreach ($lines as $line) {
      $pair = explode(' ', $line);
      $search = trim($pair[0]);
      if ($search == '') {
        continue;
      }
      if (isset($pair[1])) {
        $replace = trim($pair[1]);
        if ($replace == '' && isset($pair[2])) {
          $replace = ' ';
        }
      }
      else {
        $replace = '';
      }
      $result[$search] = $replace;
    }
    return $result;
  }

  public static function getTerm($term_name, $voc_name) {
    $term = FALSE;
    $terms = taxonomy_term_load_multiple_by_name($term_name, $voc_name);
    foreach ($terms as $term_tmp) {
      if ($term_name == $term_tmp->getName()) {
        $term = $term_tmp;
        break;
      }
    }
    return $term;
  }

  public static function getTermsField($text, $voc_name, $config) {
    $terms = [];
    $text = trim(strip_tags($text, ''));
    //rewrite hrml spcecial char to space char
    $text = str_replace("&nbsp;", ' ', $text);
    //remove empty line
    $text = preg_replace('/[\f\n\r\t\v]+ +[\f\n\r\t\v]+/', "\n", $text);
    //word wrap remove
    $text = preg_replace('/ *- *[\f\n\r\t\v]+ */', '', $text);
    //covert special char to space and clear extra any space char aka  [ \f\n\r\t\v]
    $text = preg_replace('/\s\s+/', ' ', $text);
    $text = html_entity_decode($text);
    $term_names = [];
    //safe string
    $safe_strings = Sate::getSafeString($config['safe_string']);
    foreach ($safe_strings as $safe_string) {
      if (strpos($text, $safe_string) !== FALSE) {
        $term_names[] = $safe_string;
      }
    }
    $term_names = array_unique($term_names);
    $text = str_replace($safe_strings, '', $text);
    //replace string
    $replace_strings = Sate::getReplaceString($config['replace_string']);
    $text = str_replace(array_keys($replace_strings), array_values($replace_strings), $text);
    //Punctuation marks
    $punctuation = [',-', '.', ',', '!', '?', ';', ':'];
    $text = str_replace($punctuation, ' ', $text);
    //uppercase to lowercase
    if ($config['transform_to_lower']) {
      $text = mb_strtolower($text);
    }
    $base_term = explode(' ', $text);
    module_load_include('inc', 'sate', 'sate.filter');
    $base_term = array_filter($base_term, 'sate_filter_clear_empty');
    $term_names = array_merge($term_names, $base_term);
    $term_names = array_unique($term_names);
    foreach ($term_names as &$term_name) {
      $term_name = trim($term_name);
      $term = Sate::getTerm($term_name, $voc_name);
      if (empty($term)) {
        $term = Term::create([
          'name' => $term_name,
          'vid' => $voc_name,
        ]);
        $term->save();
      }
      array_push($terms, $term);
    }
    return $terms;
  }

}
